from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

monIPsums = np.zeros((11, 12, 25, 16))  # sums of the ionospheric potential values for different years, months and regions
# we ignore the 14th region owing to its negligible contribution to the total ionospheric potential
# we include the total contributions of land, ocean and all Earth as the last three regions
monSSTs = np.zeros((11, 12))            # sums of the SST values in °C for different years and months
monNs = np.zeros((11, 12))              # number of days available for different years and months

land = [1, 2, 4, 5, 6, 7, 10, 11, 12]
ocean = [3, 8, 9, 13]
# the numbers of land and oceanic regions

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    for r in range(13):
        monIPsums[currdate.year - 2008, currdate.month - 1, :, r] += dataIP[i, 1 + r, :]
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -3] += sum(dataIP[i, r, :] for r in land)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -2] += sum(dataIP[i, r, :] for r in ocean)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -1] += dataIP[i, 0, :]
    monSSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    monNs[currdate.year - 2008, currdate.month - 1] += 1

regnames = ['Europe', 'Africa', 'Indian Ocean', 'Northern Asia', 'Southern Asia',\
    'Maritime Continent', 'Australia', 'Western Pacific Ocean', 'Eastern Pacific Ocean', 'Northern America',\
    'Middle America', 'South America', 'Atlantic Ocean', 'Land', 'Ocean', 'All Earth']
# names of the regions

IPsums = np.zeros((10, 25, 16))     # sums of the ionospheric potential values for different years and regions
SSTsums = np.zeros((10))            # sums of the SST values in °C for different years
Ns = np.zeros((10))                 # number of days available for different years

firstmon = 9    # first month to take into account (0 = Jan, 11 = Dec)
lastmon = 2     # first month to exclude from consideration (0 = Jan, 11 = Dec)

for i in range(2008, 2018):
    for m in range(firstmon, 12):
        IPsums[i - 2008, :, :] += monIPsums[i - 2008, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2008, m]
        Ns[i - 2008] += monNs[i - 2008, m]
    for m in range(lastmon):
        IPsums[i - 2008, :, :] += monIPsums[i - 2007, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2007, m]
        Ns[i - 2008] += monNs[i - 2007, m]

coeff = 240 / np.sum(np.mean(IPsums[:, :-1, -1], axis = 1), axis = 0) * np.sum(Ns)  # normalising coefficient
# the ionospheric potential is normalised such that the mean value over Dec—Feb 2008/09–2018/19 is equal to 240 kV

xmonval = np.arange(2008, 2018)
yIPmonval = np.mean(IPsums[:, :-1, :], axis = 1) / Ns[:, np.newaxis] * coeff
ySSTmonval = SSTsums / Ns

yminval = [0, 15, 24, 0, 6, 30, 3, 45, 5, 0.1, 2, 22, 6, 90, 90, 210]
ymaxval = [0.18, 21, 36, 0.12, 18, 60, 9, 75, 35, 0.4, 8, 34, 18, 150, 150, 270]
ylabels = [['0.00', '0.03', '0.06', '0.09', '0.12', '0.15', '0.18'],
    ['15', '16', '17', '18', '19', '20', '21'],
    ['24', '26', '28', '30', '32', '34', '36'],
    ['0.00', '0.02', '0.04', '0.06', '0.08', '0.10', '0.12'],
    ['6', '8', '10', '12', '14', '16', '18'],
    ['30', '35', '40', '45', '50', '55', '60'],
    ['3', '4', '5', '6', '7', '8', '9'],
    ['45', '50', '55', '60', '65', '70', '75'],
    ['5', '10', '15', '20', '25', '30', '35'],
    ['0.10', '0.15', '0.20', '0.25', '0.30', '0.35', '0.40'],
    ['2', '3', '4', '5', '6', '7', '8'],
    ['22', '24', '26', '28', '30', '32', '34'],
    ['6', '8', '10', '12', '14', '16', '18'],
    ['90', '100', '110', '120', '130', '140', '150'],
    ['90', '100', '110', '120', '130', '140', '150'],
    ['210', '220', '230', '240', '250', '260', '270']]

order = np.array([6, 12, 2, 8, 3, 9, 13, 5, 11, 7, 10, 1, 99, 4, 99, 14, 15, 16]) - 1   # order of the plots

col = [[125 / 255, 39 / 255, 126 / 255], [56 / 255, 83 / 255, 164 / 255]]               # colours of the plots

wn, hn = 3, 2   # number of subplots in horizontal (width) and vertical (height) directions
wt = 12         # total width
ht = 3.1 * hn   # total height of the main part
hb = 0.3        # height of the bottom part
fig = plt.figure(figsize = (wt, ht + hb))
ax1 = [None for _ in range(18)]
for n in range(6):
    i, j = divmod(n, 3)
    wm = 0.07   # width of margins as a fraction of the total width
    ws = 0.08   # horizontal spacing between subplots as a fraction of the total width
    rhu = 0.15  # height of the upper label as a fraction of the total subplot height (relative)
    rhl = 0.2   # height of the lower label as a fraction of the total subplot height (relative)
    w = (1 - 2 * wm - 2 * ws) / wn              # width of each subplot as a fraction of the total width
    h = ht / (ht + hb) / hn * (1 - rhu - rhl)   # height of each subplot as a fraction of the total height
    hu = ht / (ht + hb) / hn * rhu  # height of the upper label as a fraction of the total height
    hl = ht / (ht + hb) / hn * rhl  # height of the lower label as a fraction of the total height
    h0 = hb / (ht + hb)             # height of the bottom part as a fraction of the total height
    ax1[n] = fig.add_axes([wm + (w + ws) * j, h0 + hl + (h + hu + hl) * (hn - 1 - i), w, h])
    ax1[n].spines['top'].set_linewidth(0.5)
    ax1[n].spines['bottom'].set_linewidth(0.5)
    ax1[n].spines['left'].set_linewidth(0.5)
    ax1[n].spines['right'].set_linewidth(0.5)
    ax1[n].grid(False)
    ax1[n].tick_params(length = 6, width = 0.5)
    ax1[n].set_xlim((2007.5, 2017.5))
    ax1[n].set_ylim((24, 30))
    ax1[n].set_xticks(np.arange(2008, 2018))
    ax1[n].set_yticks(np.arange(24, 30.1, 1))
    ax1[n].set_xticklabels([str(y)[2:] + '/' + str(y + 1)[2:] for y in range(2008, 2018)],\
        fontsize = 'large', rotation = 270, va = 'top')
    ax1[n].set_yticklabels(['24', '25', '26', '27', '28', '29', '30'], fontsize = 'large')
    if i == 1:
        ax1[n].set_xlabel('Year', fontsize = 'large')
    if j == 0:
        ax1[n].get_yaxis().set_label_coords(-0.14, 0.5)
        ax1[n].set_ylabel('Niño 3.4 SST in Oct—Feb, °C', fontsize = 'large', color = col[0])
    r = order[n]
    corr = np.corrcoef(ySSTmonval, yIPmonval[:, r])[0, 1]
    if corr >= 0:
        ax1[n].set_title('{0:s} (r = {1:.2f})'.format(regnames[r], corr), fontsize = 'large')
    else:
        ax1[n].set_title('{0:s} (r = −{1:.2f})'.format(regnames[r], -corr), fontsize = 'large')
    ax1[n].bar(xmonval - 0.2, ySSTmonval, 0.4, color = col[0])
    ax1[n].text(-0.13, 1.07, chr(ord('a') + n), color = '0.', fontsize = 'x-large',\
        fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax1[n].transAxes)
    ax2 = ax1[n].twinx()
    ax2.spines['top'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.grid(color = '0.', linewidth = 0.5, axis = 'y')
    ax2.tick_params(length = 6, width = 0.5)
    ax2.set_ylim((yminval[r], ymaxval[r]))
    ax2.set_yticks(np.arange(yminval[r], ymaxval[r] + 0.001, (ymaxval[r] - yminval[r]) / 6.))
    ax2.set_yticklabels(ylabels[r], fontsize = 'large')
    if j == 2:
        ax2.get_yaxis().set_label_coords(1.14, 0.5)
        ax2.set_ylabel('Ion. potential in Oct—Feb, kV', fontsize = 'large', color = col[1],\
            rotation = 270, va = 'bottom')
    ax2.bar(xmonval + 0.2, yIPmonval[:, r], 0.4, color = col[1])

plt.savefig('long-term_ip_variation_oct-feb_main.eps', bbox_inches = 'tight')

wn, hn = 3, 3   # number of subplots in horizontal (width) and vertical (height) directions
wt = 12         # total width
ht = 3.1 * hn   # total height of the main part
hb = 0.3        # height of the bottom part
fig = plt.figure(figsize = (wt, ht + hb))
ax1 = [None for _ in range(18)]
for n in range(6, 15):
    i, j = divmod(n - 6, 3)
    if order[n] == 98:
        continue
    wm = 0.07   # width of margins as a fraction of the total width
    ws = 0.08   # horizontal spacing between subplots as a fraction of the total width
    rhu = 0.15  # height of the upper label as a fraction of the total subplot height (relative)
    rhl = 0.2   # height of the lower label as a fraction of the total subplot height (relative)
    w = (1 - 2 * wm - 2 * ws) / wn              # width of each subplot as a fraction of the total width
    h = ht / (ht + hb) / hn * (1 - rhu - rhl)   # height of each subplot as a fraction of the total height
    hu = ht / (ht + hb) / hn * rhu  # height of the upper label as a fraction of the total height
    hl = ht / (ht + hb) / hn * rhl  # height of the lower label as a fraction of the total height
    h0 = hb / (ht + hb)             # height of the bottom part as a fraction of the total height
    ax1[n] = fig.add_axes([wm + (w + ws) * j, h0 + hl + (h + hu + hl) * (hn - 1 - i), w, h])
    ax1[n].spines['top'].set_linewidth(0.5)
    ax1[n].spines['bottom'].set_linewidth(0.5)
    ax1[n].spines['left'].set_linewidth(0.5)
    ax1[n].spines['right'].set_linewidth(0.5)
    ax1[n].grid(False)
    ax1[n].tick_params(length = 6, width = 0.5)
    ax1[n].set_xlim((2007.5, 2017.5))
    ax1[n].set_ylim((24, 30))
    ax1[n].set_xticks(np.arange(2008, 2018))
    ax1[n].set_yticks(np.arange(24, 30.1, 1))
    ax1[n].set_xticklabels([str(y)[2:] + '/' + str(y + 1)[2:] for y in range(2008, 2018)],\
        fontsize = 'large', rotation = 270, va = 'top')
    ax1[n].set_yticklabels(['24', '25', '26', '27', '28', '29', '30'], fontsize = 'large')
    if (((i == 1) and (j != 1)) or ((i == 2) and (j == 1))):
        ax1[n].set_xlabel('Year', fontsize = 'large')
    if ((j == 0) or ((i == 2) and (j == 1))):
        ax1[n].get_yaxis().set_label_coords(-0.14, 0.5)
        ax1[n].set_ylabel('Niño 3.4 SST in Oct—Feb, °C', fontsize = 'large', color = col[0])
    r = order[n]
    corr = np.corrcoef(ySSTmonval, yIPmonval[:, r])[0, 1]
    if corr >= 0:
        ax1[n].set_title('{0:s} (r = {1:.2f})'.format(regnames[r], corr), fontsize = 'large')
    else:
        ax1[n].set_title('{0:s} (r = −{1:.2f})'.format(regnames[r], -corr), fontsize = 'large')
    ax1[n].bar(xmonval - 0.2, ySSTmonval, 0.4, color = col[0])
    if ((i == 2) and (j == 1)):
        ax1[n].text(-0.13, 1.07, chr(ord('a') + n - 7), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax1[n].transAxes)
    else:
        ax1[n].text(-0.13, 1.07, chr(ord('a') + n - 6), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax1[n].transAxes)
    ax2 = ax1[n].twinx()
    ax2.spines['top'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.grid(color = '0.', linewidth = 0.5, axis = 'y')
    ax2.tick_params(length = 6, width = 0.5)
    ax2.set_ylim((yminval[r], ymaxval[r]))
    ax2.set_yticks(np.arange(yminval[r], ymaxval[r] + 0.001, (ymaxval[r] - yminval[r]) / 6.))
    ax2.set_yticklabels(ylabels[r], fontsize = 'large')
    if ((j == 2) or ((i == 2) and (j == 1))):
        ax2.get_yaxis().set_label_coords(1.2, 0.5)
        ax2.set_ylabel('Ion. potential in Oct—Feb, kV', fontsize = 'large', color = col[1],\
            rotation = 270, va = 'bottom')
    ax2.bar(xmonval + 0.2, yIPmonval[:, r], 0.4, color = col[1])

plt.savefig('long-term_ip_variation_oct-feb_other.eps', bbox_inches = 'tight')

wn, hn = 3, 1   # number of subplots in horizontal (width) and vertical (height) directions
wt = 12         # total width
ht = 3.1 * hn   # total height of the main part
hb = 0.3        # height of the bottom part
fig = plt.figure(figsize = (wt, ht + hb))
ax1 = [None for _ in range(18)]
for n in range(15, 18):
    i, j = divmod(n - 15, 3)
    wm = 0.07   # width of margins as a fraction of the total width
    ws = 0.08   # horizontal spacing between subplots as a fraction of the total width
    rhu = 0.15  # height of the upper label as a fraction of the total subplot height (relative)
    rhl = 0.2   # height of the lower label as a fraction of the total subplot height (relative)
    w = (1 - 2 * wm - 2 * ws) / wn              # width of each subplot as a fraction of the total width
    h = ht / (ht + hb) / hn * (1 - rhu - rhl)   # height of each subplot as a fraction of the total height
    hu = ht / (ht + hb) / hn * rhu  # height of the upper label as a fraction of the total height
    hl = ht / (ht + hb) / hn * rhl  # height of the lower label as a fraction of the total height
    h0 = hb / (ht + hb)             # height of the bottom part as a fraction of the total height
    ax1[n] = fig.add_axes([wm + (w + ws) * j, h0 + hl + (h + hu + hl) * (hn - 1 - i), w, h])
    ax1[n].spines['top'].set_linewidth(0.5)
    ax1[n].spines['bottom'].set_linewidth(0.5)
    ax1[n].spines['left'].set_linewidth(0.5)
    ax1[n].spines['right'].set_linewidth(0.5)
    ax1[n].grid(False)
    ax1[n].tick_params(length = 6, width = 0.5)
    ax1[n].set_xlim((2007.5, 2017.5))
    ax1[n].set_ylim((24, 30))
    ax1[n].set_xticks(np.arange(2008, 2018))
    ax1[n].set_yticks(np.arange(24, 30.1, 1))
    ax1[n].set_xticklabels([str(y)[2:] + '/' + str(y + 1)[2:] for y in range(2008, 2018)],\
        fontsize = 'large', rotation = 270, va = 'top')
    ax1[n].set_yticklabels(['24', '25', '26', '27', '28', '29', '30'], fontsize = 'large')
    ax1[n].set_xlabel('Year', fontsize = 'large')
    if j == 0:
        ax1[n].get_yaxis().set_label_coords(-0.14, 0.5)
        ax1[n].set_ylabel('Niño 3.4 SST in Oct—Feb, °C', fontsize = 'large', color = col[0])
    r = order[n]
    corr = np.corrcoef(ySSTmonval, yIPmonval[:, r])[0, 1]
    if corr >= 0:
        ax1[n].set_title('{0:s} (r = {1:.2f})'.format(regnames[r], corr), fontsize = 'large')
    else:
        ax1[n].set_title('{0:s} (r = −{1:.2f})'.format(regnames[r], -corr), fontsize = 'large')
    ax1[n].bar(xmonval - 0.2, ySSTmonval, 0.4, color = col[0])
    ax1[n].text(-0.13, 1.07, chr(ord('a') + n - 15), color = '0.', fontsize = 'x-large',\
        fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax1[n].transAxes)
    ax2 = ax1[n].twinx()
    ax2.spines['top'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.grid(color = '0.', linewidth = 0.5, axis = 'y')
    ax2.tick_params(length = 6, width = 0.5)
    ax2.set_ylim((yminval[r], ymaxval[r]))
    ax2.set_yticks(np.arange(yminval[r], ymaxval[r] + 0.001, (ymaxval[r] - yminval[r]) / 6.))
    ax2.set_yticklabels(ylabels[r], fontsize = 'large')
    if j == 2:
        ax2.get_yaxis().set_label_coords(1.18, 0.5)
        ax2.set_ylabel('Ion. potential in Oct—Feb, kV', fontsize = 'large', color = col[1],\
            rotation = 270, va = 'bottom')
    ax2.bar(xmonval + 0.2, yIPmonval[:, r], 0.4, color = col[1])

plt.savefig('long-term_ip_variation_oct-feb_global.eps', bbox_inches = 'tight')

# plt.show()