from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

monIPsums = np.zeros((11, 12, 25, 16))  # sums of the ionospheric potential values for different years, months and regions
# we ignore the 14th region owing to its negligible contribution to the total ionospheric potential
# we include the total contributions of land, ocean and all Earth as the last three regions
monSSTs = np.zeros((11, 12))            # sums of the SST values in °C for different years and months
monNs = np.zeros((11, 12))              # number of days available for different years and months

land = [1, 2, 4, 5, 6, 7, 10, 11, 12]
ocean = [3, 8, 9, 13]
# the numbers of land and oceanic regions

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    for r in range(13):
        monIPsums[currdate.year - 2008, currdate.month - 1, :, r] += dataIP[i, 1 + r, :]
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -3] += sum(dataIP[i, r, :] for r in land)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -2] += sum(dataIP[i, r, :] for r in ocean)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -1] += dataIP[i, 0, :]
    monSSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    monNs[currdate.year - 2008, currdate.month - 1] += 1

regnames = ['Europe', 'Africa', 'Indian Ocean', 'Northern Asia', 'Southern Asia',\
    'Maritime Continent', 'Australia', 'Western Pacific Ocean', 'Eastern Pacific Ocean', 'Northern America',\
    'Middle America', 'South America', 'Atlantic Ocean', 'Land', 'Ocean', 'All Earth']
# names of the regions

IPsums = np.zeros((10, 25, 16))     # sums of the ionospheric potential values for different years and regions
SSTsums = np.zeros((10))            # sums of the SST values in °C for different years
Ns = np.zeros((10))                 # number of days available for different years

firstmon = 9    # first month to take into account (0 = Jan, 11 = Dec)
lastmon = 2     # first month to exclude from consideration (0 = Jan, 11 = Dec)

for i in range(2008, 2018):
    for m in range(firstmon, 12):
        IPsums[i - 2008, :, :] += monIPsums[i - 2008, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2008, m]
        Ns[i - 2008] += monNs[i - 2008, m]
    for m in range(lastmon):
        IPsums[i - 2008, :, :] += monIPsums[i - 2007, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2007, m]
        Ns[i - 2008] += monNs[i - 2007, m]

xmonval = np.arange(2008, 2018)
yIPmonval = (np.mean(IPsums[:, :-1, :], axis = 1) / Ns[:, np.newaxis]) /\
    (np.sum(np.mean(IPsums[:, :-1, :], axis = 1), axis = 0) / np.sum(Ns))
xSSTmonval = (SSTsums / Ns) / (np.sum(SSTsums) / np.sum(Ns))

order = np.array([6, 12, 2, 8, 3, 9, 14, 15, 16]) - 1   # order of the plots

col = [126 / 255, 20 / 255, 22 / 255]                   # colour of the plots

wn, hn = 3, 2   # number of subplots in horizontal (width) and vertical (height) directions
wt = 12         # total width
ht = 3.1 * hn   # total height of the main part
hb = 0.6        # height of the bottom part
fig = plt.figure(figsize = (wt, ht + hb))
ax = [None for _ in range(9)]
for n in range(6):
    i, j = divmod(n, 3)
    wlm = 0.08  # width of the left margin as a fraction of the total width
    wrm = 0.03  # width of the right margin as a fraction of the total width
    ws = 0.07   # horizontal spacing between subplots as a fraction of the total width
    rhu = 0.15  # height of the upper label as a fraction of the total subplot height (relative)
    rhl = 0.08  # height of the lower label as a fraction of the total subplot height (relative)
    w = (1 - wlm - wrm - 2 * ws) / wn           # width of each subplot as a fraction of the total width
    h = ht / (ht + hb) / hn * (1 - rhu - rhl)   # height of each subplot as a fraction of the total height
    hu = ht / (ht + hb) / hn * rhu  # height of the upper label as a fraction of the total height
    hl = ht / (ht + hb) / hn * rhl  # height of the lower label as a fraction of the total height
    h0 = hb / (ht + hb)             # height of the bottom part as a fraction of the total height
    ax[n] = fig.add_axes([wlm + (w + ws) * j, h0 + hl + (h + hu + hl) * (hn - 1 - i), w, h])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0.9, 1.1))
    ax[n].set_xticks(np.arange(0.9, 1.11, 0.05))
    ax[n].set_xticklabels(['0.90', '0.95', '1.00', '1.05', '1.10'], fontsize = 'large')
    r = order[n]
    if r == 8:
        ax[n].set_ylim((0.2, 1.8))
        ax[n].set_yticks(np.arange(0.2, 1.81, 0.2))
        ax[n].set_yticklabels(['0.2', '0.4', '0.6', '0.8', '1.0', '1.2', '1.4', '1.6', '1.8'], fontsize = 'large')
    else:
        ax[n].set_ylim((0.7, 1.3))
        ax[n].set_yticks(np.arange(0.7, 1.31, 0.1))
        ax[n].set_yticklabels(['0.7', '0.8', '0.9', '1.0', '1.1', '1.2', '1.3'], fontsize = 'large')
    if i == 1:
        ax[n].set_xlabel('Niño 3.4 SST in Oct—Feb as a\nfraction of the long-term mean', fontsize = 'large')
    if j == 0:
        ax[n].set_ylabel('Ion. potential in Oct—Feb as a\nfraction of the long-term mean', fontsize = 'large')
    corr = np.corrcoef(xSSTmonval, yIPmonval[:, r])[0, 1]
    if corr >= 0:
        ax[n].set_title('{0:s} (r = {1:.2f})'.format(regnames[r], corr), fontsize = 'large')
    else:
        ax[n].set_title('{0:s} (r = −{1:.2f})'.format(regnames[r], -corr), fontsize = 'large')
    a, b = np.polyfit(xSSTmonval, yIPmonval[:, r], deg = 1)
    xSSTreg = np.linspace(0.9, 1.1, 21)
    yIPreg = a * xSSTreg + b
    ax[n].plot(xSSTreg, yIPreg, '-', linewidth = 1.25, color = col)
    ax[n].plot(xSSTmonval, yIPmonval[:, r], 'o', color = col)
    ax[n].text(-0.14, 1.07, chr(ord('a') + n), color = '0.', fontsize = 'x-large',\
        fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('long-term_regression_oct-feb_main.eps', bbox_inches = 'tight')

wn, hn = 3, 1   # number of subplots in horizontal (width) and vertical (height) directions
wt = 12         # total width
ht = 3.1 * hn   # total height of the main part
hb = 0.6        # height of the bottom part
fig = plt.figure(figsize = (wt, ht + hb))
ax = [None for _ in range(9)]
for n in range(6, 9):
    i, j = divmod(n - 6, 3)
    wlm = 0.08  # width of the left margin as a fraction of the total width
    wrm = 0.03  # width of the right margin as a fraction of the total width
    ws = 0.07   # horizontal spacing between subplots as a fraction of the total width
    rhu = 0.15  # height of the upper label as a fraction of the total subplot height (relative)
    rhl = 0.08  # height of the lower label as a fraction of the total subplot height (relative)
    w = (1 - wlm - wrm - 2 * ws) / wn           # width of each subplot as a fraction of the total width
    h = ht / (ht + hb) / hn * (1 - rhu - rhl)   # height of each subplot as a fraction of the total height
    hu = ht / (ht + hb) / hn * rhu  # height of the upper label as a fraction of the total height
    hl = ht / (ht + hb) / hn * rhl  # height of the lower label as a fraction of the total height
    h0 = hb / (ht + hb)             # height of the bottom part as a fraction of the total height
    ax[n] = fig.add_axes([wlm + (w + ws) * j, h0 + hl + (h + hu + hl) * (hn - 1 - i), w, h])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0.9, 1.1))
    ax[n].set_xticks(np.arange(0.9, 1.11, 0.05))
    ax[n].set_xticklabels(['0.90', '0.95', '1.00', '1.05', '1.10'], fontsize = 'large')
    ax[n].set_ylim((0.7, 1.3))
    ax[n].set_yticks(np.arange(0.7, 1.31, 0.1))
    ax[n].set_yticklabels(['0.7', '0.8', '0.9', '1.0', '1.1', '1.2', '1.3'], fontsize = 'large')
    ax[n].set_xlabel('Niño 3.4 SST in Oct—Feb as a\nfraction of the long-term mean', fontsize = 'large')
    if j == 0:
        ax[n].set_ylabel('Ion. potential in Oct—Feb as a\nfraction of the long-term mean', fontsize = 'large')
    r = order[n]
    corr = np.corrcoef(xSSTmonval, yIPmonval[:, r])[0, 1]
    if corr >= 0:
        ax[n].set_title('{0:s} (r = {1:.2f})'.format(regnames[r], corr), fontsize = 'large')
    else:
        ax[n].set_title('{0:s} (r = −{1:.2f})'.format(regnames[r], -corr), fontsize = 'large')
    a, b = np.polyfit(xSSTmonval, yIPmonval[:, r], deg = 1)
    xSSTreg = np.linspace(0.9, 1.1, 21)
    yIPreg = a * xSSTreg + b
    ax[n].plot(xSSTreg, yIPreg, '-', linewidth = 1.25, color = col)
    ax[n].plot(xSSTmonval, yIPmonval[:, r], 'o', color = col)
    ax[n].text(-0.14, 1.07, chr(ord('a') + n - 6), color = '0.', fontsize = 'x-large',\
        fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('long-term_regression_oct-feb_global.eps', bbox_inches = 'tight')

# plt.show()