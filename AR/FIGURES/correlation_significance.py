from sys import stdin, stdout, stderr
import scipy.stats as st
from math import sqrt

stdout.write("TEST BASED ON STUDENT'S T-DISTRIBUTION\n\n")

P = 10 # the number of points
a = 0.05 # significance level

q = st.t.ppf(1 - a / 2, P - 2)
r = q / sqrt(q**2 + P - 2)

stdout.write("Significance level: {0:f}\nNumber of points (N): {1:d}\n\
Critical value (((N - 2)r^2/(1 - r)^2)^(1/2)): {2:f}\n\
Correlation coefficient(r): {3:f}\n\n".format(a, P, q, r))

P = 10 # the number of points
a = 0.01 # significance level

q = st.t.ppf(1 - a / 2, P - 2)
r = q / sqrt(q**2 + P - 2)

stdout.write("Significance level: {0:f}\nNumber of points (N): {1:d}\n\
Critical value (((N - 2)r^2/(1 - r)^2)^(1/2)): {2:f}\n\
Correlation coefficient(r): {3:f}\n".format(a, P, q, r))