from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

monIPsums = np.zeros((11, 12, 25, 14))  # sums of the ionospheric potential values for different years, months and regions
# we ignore the 14th region owing to its negligible contribution to the total ionospheric potential
# we include the total contribution of all Earth as the last region
monSSTs = np.zeros((11, 12))            # sums of the SST values in °C for different years and months
monNs = np.zeros((11, 12))              # number of days available for different years and months

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    for r in range(13):
        monIPsums[currdate.year - 2008, currdate.month - 1, :, r] += dataIP[i, 1 + r, :]
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -1] += dataIP[i, 0, :]
    monSSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    monNs[currdate.year - 2008, currdate.month - 1] += 1

IPsums = np.zeros((10, 25, 14))     # sums of the ionospheric potential values for different years and regions
SSTsums = np.zeros((10))            # sums of the SST values in °C for different years
Ns = np.zeros((10))                 # number of days available for different years

firstmon = 9    # first month to take into account (0 = Jan, 11 = Dec)
lastmon = 2     # first month to exclude from consideration (0 = Jan, 11 = Dec)

for i in range(2008, 2018):
    for m in range(firstmon, 12):
        IPsums[i - 2008, :, :] += monIPsums[i - 2008, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2008, m]
        Ns[i - 2008] += monNs[i - 2008, m]
    for m in range(lastmon):
        IPsums[i - 2008, :, :] += monIPsums[i - 2007, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2007, m]
        Ns[i - 2008] += monNs[i - 2007, m]

coeff = 240 / np.sum(np.mean(IPsums[:, :-1, -1], axis = 1), axis = 0) * np.sum(Ns)  # normalising coefficient
# the ionospheric potential is normalised such that the mean value over Decembers 2008–2018 is equal to 240 kV

totIPs = np.sum(IPsums, axis = 0) / np.sum(Ns, axis = 0) * coeff
# values of variables for all years combined

SSTanoms = SSTsums[:] / Ns[:] - np.sum(SSTsums) / np.sum(Ns)
# SST anomalies for different years

posidx = [1, 7]
negidx = [2, 3]
# numbers of years with El Niños and La Niñas

posIPs = IPsums[np.ix_(posidx, range(25), range(14))] / Ns[np.ix_(posidx)][:, np.newaxis, np.newaxis] * coeff
negIPs = IPsums[np.ix_(negidx, range(25), range(14))] / Ns[np.ix_(negidx)][:, np.newaxis, np.newaxis] * coeff
# values of variables for all years with El Niños and La Niñas

col = np.array([[128, 0, 128], [255, 0, 191], [64, 191, 159], [191, 64, 64],\
    [128, 0, 0], [0, 128, 64], [159, 191, 64], [0, 0, 255], [64, 64, 191],\
    [255, 128, 0], [191, 0, 255], [255, 0, 0], [0, 191, 255]]) / 255. # colours of the plots
order = np.array([8, 9, 3, 13, 6, 5, 7, 11, 2, 12]) - 1 # order of the plots

fig = plt.figure(figsize = (12, 12))
gs = GridSpec(nrows = 3, ncols = 6, left = 0.1, bottom = 0.1, right = 0.95, top = 0.95,\
    wspace = 0.5, hspace = 0.3, height_ratios = [3.5, 3.5, 5])
ax = [None for _ in range(7)]
for i in range(2):
    for j in range(3):
        n = 3 * i + j
        ax[n] = fig.add_subplot(gs[i, 2 * j:2 * j + 2])
        ax[n].spines['top'].set_linewidth(0.5)
        ax[n].spines['bottom'].set_linewidth(0.5)
        ax[n].spines['left'].set_linewidth(0.5)
        ax[n].spines['right'].set_linewidth(0.5)
        ax[n].grid(color = '0.', linewidth = 0.5)
        ax[n].tick_params(length = 6, width = 0.5)
        ax[n].set_xlim((0, 24))
        ax[n].set_ylim((0., 300))
        ax[n].set_xticks(np.arange(0, 25, 2))
        ax[n].set_yticks(np.arange(0., 301, 50))
        ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
        ax[n].set_yticklabels(['0', '50', '100', '150', '200', '250', '300'], fontsize = 'large')
        ax[n].text(-0.17, 1.07, chr(ord('a') + n), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
        if i == 1:
            ax[n].set_xlabel('UTC, hours', fontsize = 'large')
        if j == 0:
            ax[n].set_ylabel('Ion. potential in Oct—Feb, kV', fontsize = 'large')
            ax[n].set_title('Average over 2008/09–2017/18', fontsize = 'large')
            for k in range(10, 0, -1):
                ax[n].fill_between(np.arange(25),\
                    0, sum(totIPs[:, r] for r in order[:k]), color = col[order[k - 1]])
        elif i == 0:
            y = 2008 + posidx[j - 1]
            anom = SSTanoms[y - 2008]
            if anom >= 0:
                ax[n].set_title('{0:s}/{1:s} El Niño (+{2:.1f} °C)'.format(str(y), str(y + 1)[2:], anom),\
                    fontsize = 'large')
            else:
                ax[n].set_title('{0:s}/{1:s} El Niño (−{2:.1f} °C)'.format(str(y), str(y + 1)[2:], -anom),\
                    fontsize = 'large')
            for k in range(10, 0, -1):
                ax[n].fill_between(np.arange(25),\
                    0, sum(posIPs[j - 1, :, r] for r in order[:k]), color = col[order[k - 1]])
        else:
            y = 2008 + negidx[j - 1]
            anom = SSTanoms[y - 2008]
            if anom >= 0:
                ax[n].set_title('{0:s}/{1:s} La Niña (+{2:.1f} °C)'.format(str(y), str(y + 1)[2:], anom),\
                    fontsize = 'large')
            else:
                ax[n].set_title('{0:s}/{1:s} La Niña (−{2:.1f} °C)'.format(str(y), str(y + 1)[2:], -anom),\
                    fontsize = 'large')
            for k in range(10, 0, -1):
                ax[n].fill_between(np.arange(25),\
                    0, sum(negIPs[j - 1, :, r] for r in order[:k]), color = col[order[k - 1]])
ax[6] = fig.add_subplot(gs[2, 1:-1])
ax[6].spines['top'].set_linewidth(0.5)
ax[6].spines['bottom'].set_linewidth(0.5)
ax[6].spines['left'].set_linewidth(0.5)
ax[6].spines['right'].set_linewidth(0.5)
ax[6].grid(False)
ax[6].tick_params(top = True, right = True, which = 'both')
ax[6].tick_params(length = 6, width = 0.5)
ax[6].tick_params(length = 3, width = 0.5, which = 'minor')
ax[6].set_xlim(-180, 180)
ax[6].set_ylim(-90, 90)
ax[6].set_xticks(np.arange(-180, 181, 30))
ax[6].set_xticks(np.arange(-180, 181, 10), minor = True)
ax[6].set_yticks(np.arange(-90, 91, 30))
ax[6].set_yticks(np.arange(-90, 91, 10), minor = True)
ax[6].set_xticklabels(['180', '150W', '120W', '90W', '60W', '30W', '0', '30E', '60E', '90E', '120E', '150E', '180'],\
    color = '0.', fontsize = 'large')
ax[6].set_yticklabels(['90S', '60S', '30S', '0', '30N', '60N', '90N'], color = '0.', fontsize = 'large')
ax[6].text(-0.08, 1 + 0.07 * 3.5 / 5, chr(ord('a') + 6), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[6].transAxes)
ax[6].set_aspect(aspect = 1, adjustable = 'box')

plt.savefig('contributions_oct-feb_preliminary.eps', bbox_inches = 'tight')

col = np.array([[255, 128, 0], [0, 191, 255]]) / 255. # colours of the plots
land = np.array([1, 2, 4, 5, 6, 7, 10, 11, 12]) - 1
ocean = np.array([3, 8, 9, 13]) - 1
# the numbers of land and oceanic regions

fig = plt.figure(figsize = (12, 12))
gs = GridSpec(nrows = 3, ncols = 6, left = 0.1, bottom = 0.1, right = 0.95, top = 0.95,\
    wspace = 0.5, hspace = 0.3, height_ratios = [3.5, 3.5, 5])
ax = [None for _ in range(7)]
for i in range(2):
    for j in range(3):
        n = 3 * i + j
        ax[n] = fig.add_subplot(gs[i, 2 * j:2 * j + 2])
        ax[n].spines['top'].set_linewidth(0.5)
        ax[n].spines['bottom'].set_linewidth(0.5)
        ax[n].spines['left'].set_linewidth(0.5)
        ax[n].spines['right'].set_linewidth(0.5)
        ax[n].grid(color = '0.', linewidth = 0.5)
        ax[n].tick_params(length = 6, width = 0.5)
        ax[n].set_xlim((0, 24))
        ax[n].set_ylim((0., 300))
        ax[n].set_xticks(np.arange(0, 25, 2))
        ax[n].set_yticks(np.arange(0., 301, 50))
        ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
        ax[n].set_yticklabels(['0', '50', '100', '150', '200', '250', '300'], fontsize = 'large')
        ax[n].text(-0.17, 1.07, chr(ord('a') + n), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
        if i == 1:
            ax[n].set_xlabel('UTC, hours', fontsize = 'large')
        if j == 0:
            ax[n].set_ylabel('Ion. potential in Oct—Feb, kV', fontsize = 'large')
            ax[n].set_title('Average over 2008/09–2017/18', fontsize = 'large')
            ax[n].fill_between(np.arange(25), 0, sum(totIPs[:, r] for r in np.concatenate((land, ocean))),\
                color = col[0])
            ax[n].fill_between(np.arange(25), 0, sum(totIPs[:, r] for r in ocean), color = col[1])
        elif i == 0:
            y = 2008 + posidx[j - 1]
            anom = SSTanoms[y - 2008]
            if anom >= 0:
                ax[n].set_title('{0:s}/{1:s} El Niño (+{2:.1f} °C)'.format(str(y), str(y + 1)[2:], anom),\
                    fontsize = 'large')
            else:
                ax[n].set_title('{0:s}/{1:s} El Niño (−{2:.1f} °C)'.format(str(y), str(y + 1)[2:], -anom),\
                    fontsize = 'large')
            ax[n].fill_between(np.arange(25), 0, sum(posIPs[j - 1, :, r] for r in np.concatenate((land, ocean))),\
                color = col[0])
            ax[n].fill_between(np.arange(25), 0, sum(posIPs[j - 1, :, r] for r in ocean), color = col[1])
        else:
            y = 2008 + negidx[j - 1]
            anom = SSTanoms[y - 2008]
            if anom >= 0:
                ax[n].set_title('{0:s}/{1:s} La Niña (+{2:.1f} °C)'.format(str(y), str(y + 1)[2:], anom),\
                    fontsize = 'large')
            else:
                ax[n].set_title('{0:s}/{1:s} La Niña (−{2:.1f} °C)'.format(str(y), str(y + 1)[2:], -anom),\
                    fontsize = 'large')
            ax[n].fill_between(np.arange(25), 0, sum(negIPs[j - 1, :, r] for r in np.concatenate((land, ocean))),\
                color = col[0])
            ax[n].fill_between(np.arange(25), 0, sum(negIPs[j - 1, :, r] for r in ocean), color = col[1])
ax[6] = fig.add_subplot(gs[2, 1:-1])
ax[6].spines['top'].set_linewidth(0.5)
ax[6].spines['bottom'].set_linewidth(0.5)
ax[6].spines['left'].set_linewidth(0.5)
ax[6].spines['right'].set_linewidth(0.5)
ax[6].grid(False)
ax[6].tick_params(top = True, right = True, which = 'both')
ax[6].tick_params(length = 6, width = 0.5)
ax[6].tick_params(length = 3, width = 0.5, which = 'minor')
ax[6].set_xlim(-180, 180)
ax[6].set_ylim(-90, 90)
ax[6].set_xticks(np.arange(-180, 181, 30))
ax[6].set_xticks(np.arange(-180, 181, 10), minor = True)
ax[6].set_yticks(np.arange(-90, 91, 30))
ax[6].set_yticks(np.arange(-90, 91, 10), minor = True)
ax[6].set_xticklabels(['180', '150W', '120W', '90W', '60W', '30W', '0', '30E', '60E', '90E', '120E', '150E', '180'],\
    color = '0.', fontsize = 'large')
ax[6].set_yticklabels(['90S', '60S', '30S', '0', '30N', '60N', '90N'], color = '0.', fontsize = 'large')
ax[6].text(-0.08, 1 + 0.07 * 3.5 / 5, chr(ord('a') + 6), color = '0.', fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[6].transAxes)
ax[6].set_aspect(aspect = 1, adjustable = 'box')

plt.savefig('contributions_oct-feb_global_preliminary.eps', bbox_inches = 'tight')

fig = plt.figure(figsize = (12, 4))
ax = fig.add_subplot(1, 1, 1)
ax.spines['top'].set_linewidth(0.5)
ax.spines['bottom'].set_linewidth(0.5)
ax.spines['left'].set_linewidth(0.5)
ax.spines['right'].set_linewidth(0.5)
ax.grid(False)
ax.tick_params(length = 6, width = 0.5)
ax.set_xlim(0, 15)
ax.set_ylim(0, 1)
ax.set_xticks(np.arange(16))
ax.set_yticks([])
ax.set_xticklabels(np.arange(16), color = '0.', fontsize = 'large')
ax.set_yticklabels([], color = '0.', fontsize = 'large')

plt.savefig('contributions_labels.eps', bbox_inches = 'tight')

fig = plt.figure(figsize = (12, 4))
ax = fig.add_subplot(1, 1, 1)
ax.spines['top'].set_linewidth(0.5)
ax.spines['bottom'].set_linewidth(0.5)
ax.spines['left'].set_linewidth(0.5)
ax.spines['right'].set_linewidth(0.5)
ax.grid(False)
ax.tick_params(length = 6, width = 0.5)
ax.set_xlim(0, 15)
ax.set_ylim(0, 1)
ax.set_xticks(np.arange(0, 16, 5))
ax.set_yticks([])
ax.set_xticklabels([chr(ord('A') + i // 5) for i in np.arange(0, 16, 5)], color = '0.', fontsize = 'large')
ax.set_yticklabels([], color = '0.', fontsize = 'large')

plt.savefig('contributions_labels_global.eps', bbox_inches = 'tight')

# plt.show()