from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

monIPsums = np.zeros((11, 12, 25, 16))  # sums of the ionospheric potential values for different years, months and regions
# we ignore the 14th region owing to its negligible contribution to the total ionospheric potential
# we include the total contributions of land, ocean and all Earth as the last three regions
monSSTs = np.zeros((11, 12))            # sums of the SST values in °C for different years and months
monNs = np.zeros((11, 12))              # number of days available for different years and months

land = [1, 2, 4, 5, 6, 7, 10, 11, 12]
ocean = [3, 8, 9, 13]
# the numbers of land and oceanic regions

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    for r in range(13):
        monIPsums[currdate.year - 2008, currdate.month - 1, :, r] += dataIP[i, 1 + r, :]
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -3] += sum(dataIP[i, r, :] for r in land)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -2] += sum(dataIP[i, r, :] for r in ocean)
    monIPsums[currdate.year - 2008, currdate.month - 1, :, -1] += dataIP[i, 0, :]
    monSSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    monNs[currdate.year - 2008, currdate.month - 1] += 1

IPsums = np.zeros((10, 25, 16))     # sums of the ionospheric potential values for different years and regions
SSTsums = np.zeros((10))            # sums of the SST values in °C for different years
Ns = np.zeros((10))                 # number of days available for different years

firstmon = 9    # first month to take into account (0 = Jan, 11 = Dec)
lastmon = 2     # first month to exclude from consideration (0 = Jan, 11 = Dec)

for i in range(2008, 2018):
    for m in range(firstmon, 12):
        IPsums[i - 2008, :, :] += monIPsums[i - 2008, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2008, m]
        Ns[i - 2008] += monNs[i - 2008, m]
    for m in range(lastmon):
        IPsums[i - 2008, :, :] += monIPsums[i - 2007, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2007, m]
        Ns[i - 2008] += monNs[i - 2007, m]

coeff = 240 / np.sum(np.mean(IPsums[:, :-1, -1], axis = 1), axis = 0) * np.sum(Ns)  # normalising coefficient
# the ionospheric potential is normalised such that the mean value over Decembers 2008–2018 is equal to 240 kV

totIPs = np.sum(IPsums, axis = 0) / np.sum(Ns) * coeff
# values of variables for all years combined

posidx = [1, 7]
negidx = [2, 3]
# numbers of years with El Niños and La Niñas

posIPs = IPsums[np.ix_(posidx, range(25), range(16))] / Ns[np.ix_(posidx)][:, np.newaxis, np.newaxis] * coeff
negIPs = IPsums[np.ix_(negidx, range(25), range(16))] / Ns[np.ix_(negidx)][:, np.newaxis, np.newaxis] * coeff
# values of variables for all years with El Niños and La Niñas

stdout.write('1. 10-YEAR AVERAGE OF THE DIURNAL MEAN VALUE OF THE CONTRIBUTION OF OCEAN: {0:.1f} kV\n\n'.\
    format(np.mean(totIPs[:-1, -2])))

stdout.write('2. 10-YEAR AVERAGE OF THE DIURNAL MEAN VALUE OF THE CONTRIBUTION OF LAND: {0:.1f} kV\n\n'.\
    format(np.mean(totIPs[:-1, -3])))

stdout.write('3. CHANGES IN HOURLY VALUES OF THE CONTRIBUTION OF OCEAN DURING ENSO EVENTS:\n\n')
stdout.write('UTC hour\tAverage \tEN 09/10\tEN 15/16\tLN 10/11\tLN 11/12\n')
for h in range(24):
    stdout.write('{0:d}:00 UTC:\t{1:.1f} kV \t{2:+.1f} kV \t{3:+.1f} kV \t{4:+.1f} kV \t{5:+.1f} kV\n'.\
        format(h, totIPs[h, -2], posIPs[0, h, -2] - totIPs[h, -2], posIPs[1, h, -2] - totIPs[h, -2],\
        negIPs[0, h, -2] - totIPs[h, -2], negIPs[1, h, -2] - totIPs[h, -2]))
stdout.write('\n')

stdout.write('4. CHANGES IN HOURLY VALUES OF THE CONTRIBUTION OF LAND DURING ENSO EVENTS:\n\n')
stdout.write('UTC hour\tAverage \tEN 09/10\tEN 15/16\tLN 10/11\tLN 11/12\n')
for h in range(24):
    stdout.write('{0:d}:00 UTC:\t{1:.1f} kV \t{2:+.1f} kV \t{3:+.1f} kV \t{4:+.1f} kV \t{5:+.1f} kV\n'.\
        format(h, totIPs[h, -3], posIPs[0, h, -3] - totIPs[h, -3], posIPs[1, h, -3] - totIPs[h, -3],\
        negIPs[0, h, -3] - totIPs[h, -3], negIPs[1, h, -3] - totIPs[h, -3]))
stdout.write('\n')

stdout.write('5. NINO 3.4 SSTS AND MEAN CONTRIBUTIONS TO THE IP:\n\n')
stdout.write('Year\tSST \tOcean IP\tLand IP \tTotal IP\n')
stdout.write('Average\t{0:.1f} °C\t{1:.1f} kV\t{2:.1f} kV\t{3:.1f} kV\n'.format(np.sum(SSTsums) / np.sum(Ns),\
    np.mean(totIPs[:-1, -2]), np.mean(totIPs[:-1, -3]), np.mean(totIPs[:-1, -1])))
for y in range(2008, 2018):
    stdout.write(str(y) + '/' + str(y + 1)[2:] + '\t{0:+.1f} °C\t{1:+.1f} kV \t{2:+.1f} kV \t{3:+.1f} kV\n'.\
        format(SSTsums[y - 2008] / Ns[y - 2008] - np.sum(SSTsums) / np.sum(Ns),
        np.mean(IPsums[y - 2008, :-1, -2]) / Ns[y - 2008] * coeff - np.mean(totIPs[:-1, -2]),
        np.mean(IPsums[y - 2008, :-1, -3]) / Ns[y - 2008] * coeff - np.mean(totIPs[:-1, -3]),
        np.mean(IPsums[y - 2008, :-1, -1]) / Ns[y - 2008] * coeff - np.mean(totIPs[:-1, -1])))
stdout.write('\n')

smallregidx = np.array([1, 4, 10]) - 1
a = np.amax(np.sum(totIPs[np.ix_(range(25), smallregidx)], axis = 1))
stdout.write('6. MAXIMUM CONTRIBUTION OF REGIONS 1, 4, 10 TO THE IP IN OCT—FEB 2008/09–2017/18: {0:.2f} kV\n\n'.\
    format(a))
# the contribution of region 14 is always negligible

stdout.write('7. PARAMETERS OF THE DIURNAL VARIATION OF THE IP:\n\n')

m = np.mean(totIPs[:-1, -1])
a = np.amax(totIPs[:, -1]) - np.amin(totIPs[:, -1])
stdout.write('Oct—Feb 2008/09–2017/18:\n')
stdout.write('The diurnal mean IP value: {0:.1f} kV\n'.format(m))
stdout.write('The peak-to-peak amplitude: {0:.1f} kV\n'.format(a))
stdout.write('The peak-to-peak amplitude as a fraction of the diurnal mean: {0:.2f}\n\n'.format(a / m))

for j in range(2):
    m = np.mean(posIPs[j, :-1, -1])
    a = np.amax(posIPs[j, :, -1]) - np.amin(posIPs[j, :, -1])
    y = 2008 + posidx[j]
    stdout.write('Oct—Feb ' + str(y) + '/' + str(y + 1)[2:] + ':\n')
    stdout.write('The diurnal mean IP value: {0:.1f} kV\n'.format(m))
    stdout.write('The peak-to-peak amplitude: {0:.1f} kV\n'.format(a))
    stdout.write('The peak-to-peak amplitude as a fraction of the diurnal mean: {0:.2f}\n\n'.format(a / m))

for j in range(2):
    m = np.mean(negIPs[j, :-1, -1])
    a = np.amax(negIPs[j, :, -1]) - np.amin(negIPs[j, :, -1])
    y = 2008 + negidx[j]
    stdout.write('Oct—Feb ' + str(y) + '/' + str(y + 1)[2:] + ':\n')
    stdout.write('The diurnal mean IP value: {0:.1f} kV\n'.format(m))
    stdout.write('The peak-to-peak amplitude: {0:.1f} kV\n'.format(a))
    stdout.write('The peak-to-peak amplitude as a fraction of the diurnal mean: {0:.2f}\n\n'.format(a / m))

stdout.write('8A. CORRELATION COEFFICIENTS FOR 5-MONTH PERIODS AROUND DECEMBERS:\n\n')

yIPmonval = np.mean(IPsums[:, :-1, :], axis = 1) / Ns[:, np.newaxis] * coeff
ySSTmonval = SSTsums / Ns

corr_total = round(np.corrcoef(ySSTmonval, yIPmonval[:, -1])[0, 1], 2)
corr_land = round(np.corrcoef(ySSTmonval, yIPmonval[:, -3])[0, 1], 2)
corr_ocean = round(np.corrcoef(ySSTmonval, yIPmonval[:, -2])[0, 1], 2)
stdout.write('Total: {0:+.2f}\nLand: {1:+.2f}\nOcean: {2:+.2f}\n\n'.format(corr_total, corr_land, corr_ocean))

stdout.write('8B. CORRELATION COEFFICIENTS FOR DECEMBERS ALONE:\n\n')

IPsums = np.zeros((10, 25, 16))     # sums of the ionospheric potential values for different years and regions
SSTsums = np.zeros((10))            # sums of the SST values in °C for different years
Ns = np.zeros((10))                 # number of days available for different years

firstmon = 11    # first month to take into account (0 = Jan, 11 = Dec)
lastmon = 0     # first month to exclude from consideration (0 = Jan, 11 = Dec)

for i in range(2008, 2018):
    for m in range(firstmon, 12):
        IPsums[i - 2008, :, :] += monIPsums[i - 2008, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2008, m]
        Ns[i - 2008] += monNs[i - 2008, m]
    for m in range(lastmon):
        IPsums[i - 2008, :, :] += monIPsums[i - 2007, m, :, :]
        SSTsums[i - 2008] += monSSTs[i - 2007, m]
        Ns[i - 2008] += monNs[i - 2007, m]

coeff = 240 / np.sum(np.mean(IPsums[:, :-1, -1], axis = 1), axis = 0) * np.sum(Ns)  # normalising coefficient
# the ionospheric potential is normalised such that the mean value over Decembers 2008–2018 is equal to 240 kV

yIPmonval = np.mean(IPsums[:, :-1, :], axis = 1) / Ns[:, np.newaxis] * coeff
ySSTmonval = SSTsums / Ns

corr_total = round(np.corrcoef(ySSTmonval, yIPmonval[:, -1])[0, 1], 2)
corr_land = round(np.corrcoef(ySSTmonval, yIPmonval[:, -3])[0, 1], 2)
corr_ocean = round(np.corrcoef(ySSTmonval, yIPmonval[:, -2])[0, 1], 2)
stdout.write('Total: {0:+.2f}\nLand: {1:+.2f}\nOcean: {2:+.2f}\n'.format(corr_total, corr_land, corr_ocean))
