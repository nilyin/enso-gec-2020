from sys import stdin, stdout, stderr
import matplotlib.pyplot as plt

fig = plt.figure(figsize = (12, 4))
ax = fig.add_subplot(1, 1, 1)
ax.spines['top'].set_linewidth(0.5)
ax.spines['bottom'].set_linewidth(0.5)
ax.spines['left'].set_linewidth(0.5)
ax.spines['right'].set_linewidth(0.5)
ax.grid(False)
ax.tick_params(length = 6, width = 0.5)
ax.set_xlim(0, 15)
ax.set_ylim(0, 1)
ax.set_xticks(range(16))
ax.set_yticks([])
ax.set_xticklabels(range(16), color = '0.', fontsize = 'large')
ax.set_yticklabels([], color = '0.', fontsize = 'large')
for i in range(8):
    ax.text(0.15 + 0.1 * i, 0.5, chr(ord('a') + i), color = '0.', fontsize = 'x-large',\
        fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax.transAxes)

plt.savefig('panel_labels.eps', bbox_inches = 'tight')

# plt.show()