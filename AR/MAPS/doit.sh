#!/bin/bash

ncl ip_delta_map.ncl
ncl ip_mean_map.ncl
#ncl ip_2015_map.ncl

readarray ip_mean < mean_values.txt
readarray ip_year < year_values.txt
readarray ip_delta < delta_values.txt

for i in $(seq 0 1 23)
    do 
    export HOUR=$(printf '%02d' ${i})
    export IP_MEAN=${ip_mean[i]::-3}      
    export IP_DELTA=${ip_delta[i]::-1}      
    echo ${HOUR}
    echo ${IP_MEAN}
    echo ${IP_DELTA}
    ncl ip_delta_hour.ncl
    ncl ip_mean_hour.ncl
    done

