#!/usr/bin/env python3
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import numpy as np
from datetime import datetime, timedelta
from calendar import monthrange, isleap 

os.system('rm *.bin')
os.system('rm *.txt')

years = [ '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', 'mean',  'ENSO' , '<_ENSO' , '>_ENSO']
y_labels = [':r',   ':g',   ':b',   ':c',  '--r',  '--g',  '--b',  '--c',  '-.r',  '-.g', '-.b' , '-k'  ,   '-k'  ,  ':k'    ,   '--k'  ]
line_w =  [   2,       2,      2,      2,      2,      2,      2,      2,      2,      2,      2,      6,        3,         3,     3   ]
months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
eng_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

land_index = 0
plt.rcParams.update({'font.size' : 13})
full_ip_map = np.load('FULL-IP-MAP.npy')
ds = np.load('ds.npy')


def main():
    ip_mean_map = np.zeros((25,180,360))
    number_all = 0
    for i_year in range(10):
        f_idx, l_idx = get_5m_period_new(years[i_year])
        for i_day in range(f_idx, l_idx + 1):
            number_all += 1
            ip_mean_map += full_ip_map[i_day] 
    ip_mean_map /= number_all 

    ip_mean_map = ip_mean_map[:24,:,:]

    ip_norma = np.mean(np.sum(ip_mean_map, axis=(1,2))) / 240000
#    ip_norma = np.max(np.max(ip_mean_map, axis=0))/100     # normalize 
#    ip_norma = np.max(np.mean(ip_mean_map, axis=0))/100     # normalize 

    ip_mean_map /= ip_norma

    np.mean(ip_mean_map, axis=0).tofile('IP_MAP_MEAN.bin')
    print('Mean values')
    mean_values_file = open('mean_values.txt', 'a')
    year_values_file = open('year_values.txt', 'a')
    delta_values_file = open('delta_values.txt', 'a')

    for i_time in range(24):
        np.abs(ip_mean_map[i_time]).tofile('IP_MAP_'+ str(i_time).zfill(2) + '.bin')  
        ip_value = round(0.05 + np.sum(ip_mean_map[i_time]) / 1000, 1)
        print('Hour:', i_time, ip_value)
        mean_values_file.write(str(ip_value)+'\n')

    print('Year values')

    for i_year in [7]:
        print(years[i_year])
        ip_mean_map_y = np.zeros((25,180,360))
        number_y = 0
        f_idx, l_idx = get_5m_period_new(years[i_year])
        for i_day in range(f_idx, l_idx + 1):
            number_y += 1
            ip_mean_map_y += full_ip_map[i_day] 
        ip_mean_map_y /= number_y 
        ip_mean_map_y = ip_mean_map_y[:24,:,:]

        ip_norma_y = np.mean(np.sum(ip_mean_map_y, axis=(1,2))) / 240000

#        ip_mean_map_y /= ip_norma_y
        ip_mean_map_y /= ip_norma       # normalize by 10 periods mean

        ip_mean_map_y_delta = ip_mean_map_y - ip_mean_map

        print(np.sum(ip_mean_map_y_delta)/24)

        np.mean(ip_mean_map_y_delta, axis=0).tofile('IP_MAP_DELTA_'+ years[i_year] + '.bin')  
        np.mean(ip_mean_map_y, axis=0).tofile('IP_MAP_MEAN_'+ years[i_year] + '.bin')  

        for i_time in range(24):
            ip_mean_map_y_delta[i_time].tofile('IP_MAP_DELTA_'+ years[i_year] +'_'+ str(i_time).zfill(2) + '.bin')  
            ip_mean_map_y[i_time].tofile('IP_MAP_MEAN_'+ years[i_year] +'_'+ str(i_time).zfill(2) + '.bin')  
            ip_value_y = round(np.sum(ip_mean_map_y[i_time]) / 1000, 1)
            print('Hour:', i_time, ip_value_y)
            year_values_file.write(str(ip_value_y)+'\n')
            ip_delta = round((np.sum(ip_mean_map_y[i_time]) - np.sum(ip_mean_map[i_time]))/1000,1)
            print(ip_delta)
            if ip_delta > 0:
                ip_delta = '~F29~+~F~' + str(ip_delta)
            elif ip_delta == 0:
                ip_delta = '0'
            elif ip_delta < 0:
                ip_delta = '~F29~-~F~' + str(abs(ip_delta))
            print(ip_delta)
            delta_values_file.write(ip_delta+'\n')
            

def get_5m_period_new(year):
    if year == '2008':
        first_index = 93
        last_index = 142
    elif year == '2009':
        first_index = 214
        last_index = 264
    elif year == '2010':
        first_index = 336
        last_index = 385
    elif year == '2011':
        first_index = 458
        last_index = 507
    elif year == '2012':
        first_index = 580
        last_index = 629
    elif year == '2013':
        first_index = 701
        last_index = 751
    elif year == '2014':
        first_index = 823
        last_index = 872
    elif year == '2015':
        first_index = 954
        last_index = 994
    elif year == '2016':
        first_index = 1067
        last_index = 1116
    elif year == '2017':
        first_index = 1188
        last_index = 1238
    return(first_index, last_index)


def get_5m_period(year):
    current_month = datetime.strptime('10' + year, '%m%Y')
    first_index = day2index(current_month)
    add_days = 152 if isleap(int(year)+1) else 151
    last_index = day2index(current_month + timedelta(days=add_days))
    return(first_index, last_index)


def get_month(month, year):
    current_month = datetime.strptime(month + year, '%m%Y')
    month_len = monthrange(current_month.year, current_month.month)[1] 
    first_index = day2index(current_month)    
    last_index = day2index(current_month + timedelta(days=month_len))    
    return (first_index, last_index)


def get_year(year):
    if year == 'mean':
        return(0, day2index(datetime(2018,12,31)))
    if year == 'ENSO':
        return(day2index(datetime(2015,7,1)), day2index(datetime(2016,6,30)))
    if year == '<_ENSO':
        return(0, day2index(datetime(2015,6,30)))
    if year == '>_ENSO':
        return(day2index(datetime(2016,7,1)), day2index(datetime(2018,12,31)))
    year_len = 366 if isleap(int(year)) else 365 
    year = datetime.strptime(year, '%Y')
    first_index = day2index(year)
    last_index = day2index(year + timedelta(days=year_len))
    return (first_index, last_index)


def day2index(day):
    ref_day = datetime(2008,1,1)    
    index = int( (day - ref_day).days // 3 )
    return index
    

if __name__ == '__main__':
    for i in range(10):
        print(get_5m_period(years[i]))
        print(get_5m_period_new(years[i]))


    main()
