#!/usr/bin/env python3
from threading import Thread
import numpy as np
from dateutil.rrule import rrule, MONTHLY
from datetime import date, timedelta
from calendar import monthrange
import os


def main():
    results = np.zeros(((8*365+3*366) // 3 + 1,25,180,360))
    startday, endday = date(2007,12,31), date(2018,12,30)
    day_number = (endday - startday).days // 3 + 1 
    print(day_number)
    thread_number = 20
    day = startday
    index = 0
    for block in range(day_number // thread_number):
        threads = [None] * thread_number
        for  i in range(thread_number):
            threads[i] = Thread(target = day_ip, args=(day, results, index))
            threads[i].start()
            day += timedelta(days=3)
            index += 1
        for  i in range(thread_number):
            threads[i].join() 
    np.save('FULL-IP-MAP', results)


def main_sst():
    sst = np.zeros(((8*365+3*366) // 3 + 1,25,180,360))
    startday, endday = date(2007,12,31), date(2018,12,30)
    day_number = (endday - startday).days // 3 + 1 
    print(day_number)
    thread_number = 20
    day = startday
    index = 0
    for block in range(day_number // thread_number):
        threads = [None] * thread_number
        for  i in range(thread_number):
            threads[i] = Thread(target = day_sst, args=(day, sst, index))
            threads[i].start()
            day += timedelta(days=3)
            index += 1
        for  i in range(thread_number):
            threads[i].join() 
    np.save('SST-MAP', sst)


def correcter(array):
    delta = array[42] - array[18]
    result = np.zeros(len(array)) 
    for i in range(len(array)):
        result[i] = array[i] - ((i - 30)/24)*delta
    return result 


def day_sst(day, results, index):
    print(day)
    day_sst = np.zeros((25,180,360))
    if date(2007,12,31) <= day <= date(2018,12,30):
        for hour in ['00', '12']:
            sst = np.zeros((49,180,360))        
            datafile = os.path.join(os.getenv('PWD'),'data',day.strftime('DATA-%Y-%m-%d-' + hour + '.npz')) 
            sst = np.load(datafile)['tsk'] - 273.15

            if hour == '00':
                day_sst[:18] += sst[24:42]
                day_sst[18:] += sst[18:25]
            elif hour == '12':
                day_sst[6:] += sst[18:37]
                day_sst[:6] += sst[36:42]

    results[index] = day_sst/2
    

def day_ip(day,results,index):
    print(day)
    result = np.zeros((25,180,360))
    if date(2007,12,31) <= day <= date(2018,12,30):
        dtime,lat,lon = 49,180,360
        ip_result = np.zeros((dtime,lat,lon))
        ds = np.load('ds.npy')
        h0 = 6000
        for hour in ['00', '12']:
            datafile = os.path.join(os.getenv('PWD'),'data',day.strftime('DATA-%Y-%m-%d-' + hour + '.npz')) 
            rp2h = np.zeros((dtime,lat,lon))
            cape = np.load(datafile)['mcape']
            ctop = np.load(datafile)['ctop']
            cbot = np.load(datafile)['cbot']
            rainc = np.load(datafile)['rainc']
            rainnc = np.load(datafile)['rainnc']
            rain = rainc
            prwa = np.load(datafile)['pw']
            rp2h[1:-1,:,:] = rain[2:,:,:] - rain[:-2,:,:]          # rain per 2 hours  

            ip_result = ds * (np.exp(-cbot/h0) - np.exp(-ctop/h0)) * (rp2h / prwa) * np.heaviside(cape - 1000, 1)
            delta = ip_result[42] - ip_result[18]
            for i in range(dtime):
                ip_result[i] -= delta * ((i - 30)/24) 

            if hour == '00':
                result[:18] += ip_result[24:42]
                result[18:] += ip_result[18:25]
            elif hour == '12':
                result[6:] += ip_result[18:37]
                result[:6] += ip_result[36:42]
    results[index] = result/2


def month_ip(month):
    print(month.year, month.month)
    day_range = monthrange(month.year, month.month)[1] // 3
    results = np.zeros((day_range,17,25))
    threads = [None] * day_range
    for day_index in range(day_range):
        threads[day_index] = Thread(target = day_ip, args=(month+timedelta(days=-1), results, day_index))
        threads[day_index].start()
        month += timedelta(days=3) 
    for day_index in range(day_range):
        threads[day_index].join()
    mean_ip = np.mean(results,axis=0)
    return mean_ip


def month_saver():
    
    start_date = date(2008,1,1)
    for month in rrule(freq=MONTHLY, count=12, dtstart=start_date):
        ip = month_ip(date(month.year, month.month, month.day))
        np.save('IP-' + month.strftime("%Y-%m"), ip)

if __name__ == '__main__':
#    month_ip(date(2016,2,1))    
    main_sst()
#    main()
#    month_saver()
