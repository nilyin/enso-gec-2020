#!/usr/bin/env python3
import subprocess   
import os 
from datetime import datetime, timedelta

startday = datetime(2007,12,31,0)
endday = datetime(2018,12,30,12)
day_gap = 2

day = startday
day_number = 1 + int( ((endday - startday).days) // (day_gap + 1 ) )    

print(day_number)
thread_number = 10  

for i_block in range(day_number // thread_number):
    threads = []
    for i in range(thread_number):
        for j in range(2):
            threads.append(subprocess.Popen(['./thread.py', day.strftime('%Y'), day.strftime('%m'), day.strftime('%d'), day.strftime('%H')]))    
            day += timedelta(hours=12)
        day += timedelta(days=day_gap)
    for i in range(2 * thread_number):
        threads[i].wait()
