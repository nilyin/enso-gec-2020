#!/usr/bin/env python3
from netCDF4 import Dataset
from wrf import getvar
import os 
import sys 
import numpy as np
from datetime import datetime, timedelta
import gzip

eventday = datetime(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]))
print(eventday)
if datetime(2007,12,31,0) <= eventday <= datetime(2018,12,30,12):
    t_bottom = 0.           # Tempmerature of cloud botton in C
    t_top = -38.            # Temperature of cloud top in C   
#    data_type = 'DS083.3'   # GDAS data
    data_type = 'DS083.2'   # GDAS data
#    resolution = '0.25'
    resolution = '1.0'

    nlat,nlon = int(180/float(resolution)), int(360/float(resolution))   
    timeoffset, timeduration = 0, 49
    mainpath = os.path.join(os.getenv('HOME'),'wrfmain', 'global-' + resolution, 'RDA_' + data_type, eventday.strftime('%Y%m%d%H'))  
    c_top      = np.zeros([timeduration, nlat, nlon])
    c_bottom   = np.zeros([timeduration, nlat, nlon])
    timeindex  = range(timeoffset, timeoffset + timeduration)

    savefile = os.path.join(mainpath, eventday.strftime('DATA-%Y-%m-%d-%H'))    
    savefile_2 = os.path.join(os.getenv('PWD'), 'data', eventday.strftime('DATA-%Y-%m-%d-%H'))    
    wrffile  = os.path.join(mainpath, eventday.strftime('wrfout_d01_%Y-%m-%d_%H:00:00'))

    ncfile = Dataset(wrffile)
    sst = np.array(getvar(ncfile, 'SST', timeidx = 0, meta=False))
    tsk = np.array(getvar(ncfile, 'TSK', timeidx = timeindex, meta=False))
    rainc = np.array(getvar(ncfile, 'RAINC', timeidx = timeindex, meta=False))
    rainnc = np.array(getvar(ncfile, 'RAINNC', timeidx = timeindex, meta=False))
    pw = np.array(getvar(ncfile, 'pw', timeidx = timeindex, meta=False))
    maskcape_2d = getvar(ncfile, 'cape_2d',  timeidx=timeindex, meta=False)
    maskcape_3d = getvar(ncfile, 'cape_3d',  timeidx=timeindex, meta=False)
    maskcape_2d = maskcape_2d.filled(fill_value=0)
    maskcape_3d = maskcape_3d.filled(fill_value=0)
    mcape = np.array(maskcape_2d[0,:,:,:])      
    cape = np.array(maskcape_3d[0,:,0,:,:])      
    z = getvar(ncfile, 'z', units='m', meta=False)
    tk = getvar(ncfile, 'tk', timeidx=timeindex, meta=False)
    tc_bot = tk - (273.15 + t_bottom)
    tc_top = tk - (273.15 + t_top)
    for i in range(tk.shape[0]):
        t0_b = np.sign(tc_bot[i])
        t0_bix = np.argmax(t0_b < 0, axis=0)
        h_b = z.reshape(z.shape[0]*nlat*nlon)[t0_bix.reshape(nlat*nlon)*nlat*nlon + np.arange(nlat*nlon)]
        c_bottom[i] = h_b.reshape(nlat,nlon) 
               
        t0_t = np.sign(tc_top[i])
        t0_tix = np.argmax(t0_t < 0, axis=0 )
        h_t = z.reshape(z.shape[0]*nlat*nlon)[t0_tix.reshape(nlat*nlon)*nlat*nlon + np.arange(nlat*nlon)]
        c_top[i] = h_t.reshape(nlat,nlon) 
    np.savez_compressed(savefile, cape = cape, mcape = mcape, rainc = rainc, rainnc = rainnc, pw = pw, ctop = c_top, cbot = c_bottom, stt = sst, tsk = tsk)
    np.savez_compressed(savefile_2, cape = cape, mcape = mcape, rainc = rainc, rainnc = rainnc, pw = pw, ctop = c_top, cbot = c_bottom, sst = sst, tsk = tsk)

