from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

IPs = np.zeros((11, 12, 25))    # sums of the IP values for different years and months
IPsqrs = np.zeros((11, 12, 25)) # sums of the squares of the IP values for different years and months
Ns = np.zeros((11, 12))         # number of days available for different years and months

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    IPs[currdate.year - 2008, currdate.month - 1, :] += dataIP[i, 0, :]
    IPsqrs[currdate.year - 2008, currdate.month - 1, :] += dataIP[i, 0, :]**2
    Ns[currdate.year - 2008, currdate.month - 1] += 1

M = 5 # the number of months used for averaging (M needs to be odd)

linIPs = IPs.reshape((11 * 12, 25))
linIPsqrs = IPsqrs.reshape((11 * 12, 25))
linNs = Ns.reshape((11 * 12))
# linear reshapes of the main arrays with respect to months

linIPsum = np.insert(np.cumsum(linIPs, axis = 0), 0, 0, axis = 0)
linIPsqrsum = np.insert(np.cumsum(linIPsqrs, axis = 0), 0, 0, axis = 0)
linNsum = np.insert(np.cumsum(linNs), 0, 0)
# cumulative sums over months

IPsums = linIPsum[M:, :] - linIPsum[:-M, :]
IPsqrsums = linIPsqrsum[M:, :] - linIPsqrsum[:-M, :]
Nsums = linNsum[M:] - linNsum[:-M]
IPmeans = IPsums[:, :] / Nsums[:, np.newaxis]
# sums and mean values over intervals of length M

IPerrs = IPsqrsums[:, :] - 2 * IPsums[:, :] * IPmeans[:, :] + Nsums[:, np.newaxis] * IPmeans[:, :]**2
IPerrs = np.power(IPerrs[:, :], 0.5) / np.power(Nsums[:, np.newaxis] * (Nsums[:, np.newaxis] - 1), 0.5)
# standard errors of the IP for different years and months

IPerrs[:, :] /= np.mean(IPmeans[:, :-1, np.newaxis], axis = 1)
IPmeans[:, :] /= np.mean(IPmeans[:, :-1, np.newaxis], axis = 1)
# passing to relative values of the IP

totIPs = np.concatenate([np.sum(IPs, axis = 0) for _ in range(11)], axis = 0)
totIPsqrs = np.concatenate([np.sum(IPsqrs, axis = 0) for _ in range(11)], axis = 0)
totNs = np.concatenate([np.sum(Ns, axis = 0) for _ in range(11)])
# values of variables for all years combined

totIPsum = np.insert(np.cumsum(totIPs, axis = 0), 0, 0, axis = 0)
totIPsqrsum = np.insert(np.cumsum(totIPsqrs, axis = 0), 0, 0, axis = 0)
totNsum = np.insert(np.cumsum(totNs), 0, 0)
# cumulative sums over months for all years combined

totIPsums = totIPsum[M:, :] - totIPsum[:-M, :]
totIPsqrsums = totIPsqrsum[M:, :] - totIPsqrsum[:-M, :]
totNsums = totNsum[M:] - totNsum[:-M]
totIPmeans = totIPsums[:, :] / totNsums[:, np.newaxis]
# sums and mean values over intervals of length M for all years combined

totIPerrs = totIPsqrsums[:, :] - 2 * totIPsums[:, :] * totIPmeans[:, :] + totNsums[:, np.newaxis] * totIPmeans[:, :]**2
totIPerrs = np.power(totIPerrs[:, :], 0.5) / np.power(totNsums[:, np.newaxis] * (totNsums[:, np.newaxis] - 1), 0.5)
# standard errors of the IP for all years combined

totIPerrs[:, :] /= np.mean(totIPmeans[:, :-1, np.newaxis], axis = 1)
totIPmeans[:, :] /= np.mean(totIPmeans[:, :-1, np.newaxis], axis = 1)
# passing to relative values of the IP

nw, nh = 1, 1   # number of subplots in horizontal (width) and vertical (height) directions
w = 5.0         # width of each subplot
wt = 0.4        # width of tick labels
wl = 0.2        # width of each axis label
ws = 0.4        # horizontal spacing between subplots
h = [4.0]       # heights of subplots in different rows
assert len(h) == nh
ht = 0.2        # height of tick labels
hl = 0.2        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.2        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt) + wl + (nw - 1) * ws + 2 * wm        # total width
hm = 0.2        # height of margins
th = sum(h[i] for i in range(nh)) + nh * (ht + hu) + hl + (nh - 1) * hs + 2 * hm        # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
for n in range(nw * nh):
    i, j = divmod(n, nw)
    ax[n] = fig.add_axes([(wm + wl + j * (w + ws) + (j + 1) * wt) / tw,\
        (hm + hl + sum(h[nh - 1 - i] for i in range(nh - 1 - i)) + (nh - 1 - i) * (hu + hs) + (nh - i) * ht) / th,\
        w / tw, h[i] / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    ax[n].set_xlim((0, 24))
    ax[n].set_xticks(np.arange(0, 25, 2))
    ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
    ax[n].set_xlabel('UTC, hours', fontsize = 'large')
    ax[n].set_ylim((0.85, 1.2))
    ax[n].set_yticks(np.arange(0.85, 1.21, 0.05))
    ax[n].set_yticklabels(['0.85', '0.90', '0.95', '1.00', '1.05', '1.10', '1.15', '1.20'], fontsize = 'large')
    ax[n].set_ylabel('Ionospheric potential as a fraction of the mean', fontsize = 'large')
    xval = np.arange(25)
    y = 7   # number of the year (from 0 to 10)
    yIP1val = IPmeans[9 + 12 * y, :]
    yIP2val = totIPmeans[9, :]
    dyIP1val = IPerrs[9 + 12 * y, :]
    dyIP2val = totIPerrs[9, :]
    ax[n].plot(xval, yIP1val, color = [0.5, 0., 0.5], clip_on = False, zorder = 7)
    ax[n].plot(xval, yIP1val - dyIP1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 5)
    ax[n].plot(xval, yIP1val + dyIP1val, color = [0.5, 0., 0.5], linestyle = 'dotted', clip_on = False, zorder = 5)
    ax[n].plot(xval, yIP2val, color = [0., 0., 1.], clip_on = False, zorder = 6)
    ax[n].plot(xval, yIP2val - dyIP2val, color = [0., 0., 1], linestyle = 'dotted', clip_on = False, zorder = 4)
    ax[n].plot(xval, yIP2val + dyIP2val, color = [0., 0., 1.], linestyle = 'dotted', clip_on = False, zorder = 4)
    ax[n].set_title('Oct—Feb, 2008–2018 vs ' + str(2008 + y) + '/' + str(2009 + y), fontsize = 'large')

plt.savefig('ip_variation_2015_5_months.eps')

