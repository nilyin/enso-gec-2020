from sys import stdin, stdout, stderr
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

IPs = np.zeros((11, 12, 25)) # IP values for different years and months
SSTs = np.zeros((11, 12))    # Niño 3.4 SST values for different years and months
Ns = np.zeros((11, 12))      # number of days available for different years and months

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    IPs[currdate.year - 2008, currdate.month - 1, :] += dataIP[i, 0, :]
    SSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    Ns[currdate.year - 2008, currdate.month - 1] += 1

M = 5 # the number of months used for averaging

linIPs = IPs.reshape((11 * 12, 25))
linSSTs = SSTs.reshape((11 * 12))
linNs = Ns.reshape((11 * 12))
# linear reshapes of the main arrays with respect to months

linIPsum = np.insert(np.cumsum(linIPs, axis = 0), 0, 0, axis = 0)
linSSTsum = np.insert(np.cumsum(linSSTs), 0, 0)
linNsum = np.insert(np.cumsum(linNs), 0, 0)
# cumulative sums over months

IPmeans = (linIPsum[M:, :] - linIPsum[:-M, :]) / (linNsum[M:, np.newaxis] - linNsum[:-M, np.newaxis])
SSTmeans = (linSSTsum[M:] - linSSTsum[:-M]) / (linNsum[M:] - linNsum[:-M])
# mean values over intervals of length M

totIPs = np.concatenate([np.sum(IPs, axis = 0) for _ in range(11)], axis = 0)
totSSTs = np.concatenate([np.sum(SSTs, axis = 0) for _ in range(11)])
totNs = np.concatenate([np.sum(Ns, axis = 0) for _ in range(11)])
# values of variables for all years combined

totIPsum = np.insert(np.cumsum(totIPs, axis = 0), 0, 0, axis = 0)
totSSTsum = np.insert(np.cumsum(totSSTs), 0, 0)
totNsum = np.insert(np.cumsum(totNs), 0, 0)
# cumulative sums over months for all years combined

totIPmeans = (totIPsum[M:, :] - totIPsum[:-M, :]) / (totNsum[M:, np.newaxis] - totNsum[:-M, np.newaxis])
totSSTmeans = (totSSTsum[M:] - totSSTsum[:-M]) / (totNsum[M:] - totNsum[:-M])
# mean values over intervals of length M for all years combined

IPmeans[:, :] /= np.mean(IPmeans[:, :-1, np.newaxis], axis = 1)
totIPmeans[:, :] /= np.mean(totIPmeans[:, :-1, np.newaxis], axis = 1)
# passing to relative values of the IP

monIPs = np.mean(IPs[:, :, :-1], axis = 2) / Ns[:, :]
montotIPs = np.mean(totIPs[:, :-1], axis = 1) / totNs[:]
# monthly values

xval = np.arange(M / 2, 11 * 12 - (M - 1) / 2)
yIP1val = IPmeans[:, 13] - totIPmeans[:, 13]
yIP2val = IPmeans[:, 21] - totIPmeans[:, 21]
ySSTval = SSTmeans - totSSTmeans
yzeroval = np.zeros(11 * 12 - 2 * (M // 2))
pos = np.logical_or(np.logical_and(np.logical_and(xval > 12, xval < 3 * 12), ySSTval > 0),\
    np.logical_and(np.logical_and(xval > 6 * 12 - 6, xval < 9 * 12 - 6), ySSTval > 0))
neg = np.logical_and(np.logical_and(xval > 2 * 12, xval < 5 * 12 - 6), ySSTval < 0)

xmonval = np.arange(2008, 2019)
yIPmonval = monIPs[:, 11] / montotIPs[11] * 240
ySSTmonval = SSTs[:, 11] / Ns[:, 11]

ycorrval = []
for h in range(0, 25):
    corr = np.corrcoef(IPmeans[:, h] - totIPmeans[:, h], SSTmeans - totSSTmeans)[0, 1]
    ycorrval.append(corr)

stdout.write("Correlation coefficient for monthly values in December: {0:f}\n".format(np.corrcoef(ySSTmonval, yIPmonval)[0, 1]))
stdout.write("Correlation coefficient for the anomaly at 13:00 UTC: {0:f}\n".format(np.corrcoef(ySSTval, yIP1val)[0, 1]))
stdout.write("Correlation coefficient for the anomaly at 21:00 UTC: {0:f}\n".format(np.corrcoef(ySSTval, yIP2val)[0, 1]))

nw, nh = 2, 2   # number of subplots in horizontal (width) and vertical (height) directions
w = 3.3         # width of each subplot
w2 = 3.8        # width of a large subplot
wt1 = 0.3       # width of small tick labels
wt2 = 0.5       # width of large tick labels
wl1 = 0.2       # width of each small axis label
wl2 = 0.5       # width of each large axis label
ws = 0.4        # horizontal spacing between subplots
h = 3.3         # height of each subplot
ht = 0.2        # height of tick labels
hl = 0.2        # height of each axis label
hs = 0.2        # vertical spacing between subplots
hu = 0.1        # height of each subplot label
wm = 0.2        # width of margins
tw = nw * (w + wt1 + wt2) + 3 * wl1 + wl2 + (nw - 1) * ws + 2 * wm      # total width
hm = 0.2        # height of margins
th = nh * (h + hl + ht + hu) + (nh - 1) * hs + 2 * hm       # total height
fig = plt.figure(figsize = (tw, th))
ax = [None for _ in range(nw * nh)]
for n in range(nw * nh):
    i, j = divmod(n, nw)
    if n == 0:
        ax[n] = fig.add_axes([(wm + wl1 + wt2) / tw, (hm + 2 * hl + 2 * ht + h + hu + hs) / th,\
            w2 / tw, h / th])
    elif n == 2:
        ax[n] = fig.add_axes([(wm + wl1 + wt1) / tw, (hm + hl + ht) / th,\
            w / tw, h / th])
    else:
        ax[n] = fig.add_axes([(wm + 3 * wl1 + 2 * wt1 + wt2 + w + ws) / tw,\
            (hm + (nh - i) * (hl + ht) + (nh - 1 - i) * (h + hu + hs)) / th,\
            w / tw, h / th])
    ax[n].spines['top'].set_linewidth(0.5)
    ax[n].spines['bottom'].set_linewidth(0.5)
    ax[n].spines['left'].set_linewidth(0.5)
    ax[n].spines['right'].set_linewidth(0.5)
    if n == 0:
        ax[n].grid(color = '0.', linewidth = 0.5, axis = 'y')
    elif n == 2:
        ax[n].grid(False)
    else:
        ax[n].grid(color = '0.', linewidth = 0.5)
    ax[n].tick_params(length = 6, width = 0.5)
    if n == 0:
        ax[n].set_xlim((-0.5, 24.5))
        ax[n].set_xticks(np.arange(0, 25, 2))
        ax[n].set_xticklabels(np.arange(0, 25, 2), fontsize = 'large')
        ax[n].set_xlabel('UTC, hours', fontsize = 'large')
        ax[n].set_ylim((-1, 1))
        ax[n].set_yticks(np.arange(-1, 1.1, 0.2))
        ax[n].set_yticklabels(['−1.0', '−0.8', '−0.6', '−0.4', '−0.2', '0.0',\
            '+0.2', '+0.4', '+0.6', '+0.8', '+1.0'], fontsize = 'large')
        ax[n].get_yaxis().set_label_coords(-(0.12 * w + wt2 - wt1) / w2, 0.5)
        ax[n].set_ylabel('Correlation coefficient', fontsize = 'large')
        ax[n].bar(np.arange(0, 25), ycorrval, color = [0., 0.5, 0.])
    elif n == 2:
        ax[n].set_xlim((2007.5, 2018.5))
        ax[n].set_xticks(np.arange(2008, 2019))
        ax[n].set_xticklabels([str(y)[2:] for y in range(2008, 2019)], fontsize = 'large')
        ax[n].set_xlabel('Year', fontsize = 'large')
        ax[n].set_ylim((24, 30))
        ax[n].set_yticks(np.arange(24, 30.1, 1))
        ax[n].set_yticklabels(['24', '25', '26', '27', '28', '29', '30'], fontsize = 'large')
        ax[n].get_yaxis().set_label_coords(-0.12, 0.5)
        ax[n].set_ylabel('Niño 3.4 SST in December, °C', fontsize = 'large', color = [0.5, 0., 0.5])
        ax1 = ax[n].twinx()
        ax1.spines['top'].set_visible(False)
        ax1.spines['bottom'].set_visible(False)
        ax1.spines['left'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        ax1.grid(color = '0.', linewidth = 0.5, axis = 'y')
        ax1.tick_params(length = 6, width = 0.5)
        ax1.set_ylim((210, 270))
        ax1.set_yticks(np.arange(210, 271, 10))
        ax1.set_yticklabels(np.arange(210, 271, 10), fontsize = 'large')
        ax1.set_ylabel('Ionospheric potential in December, kV', fontsize = 'large',\
            color = [0., 0., 1.], rotation = 270, va = 'bottom')
        ax[n].bar(xmonval - 0.2, ySSTmonval, 0.4, color = [0.5, 0., 0.5])
        ax1.bar(xmonval + 0.2, yIPmonval, 0.4, color = [0., 0., 1.])
    else:
        ax[n].set_xlim((0, 11 * 12))
        ax[n].set_xticks([12 * k for k in range(11 + 1)])
        ax[n].set_xticks([6 + 12 * k for k in range(11)], minor = True)
        for tick in ax[n].xaxis.get_minor_ticks():
            tick.tick1line.set_markersize(0)
            tick.tick2line.set_markersize(0)
        ax[n].set_xticklabels([])
        ax[n].set_xticklabels([str(y)[2:] for y in range(2008, 2019)], fontsize = 'large', minor = True)
        tr = []
        for label in ax[n].xaxis.get_majorticklabels():
            tr.append(label.get_transform())
        for label in ax[n].xaxis.get_minorticklabels():
            label.set_transform(tr[0])
        ax[n].set_xlabel('Year', fontsize = 'large')
        ax[n].set_ylim((-3, 3))
        ax[n].set_yticks(np.arange(-3, 3.1, 1))
        ax[n].set_yticklabels(['−3', '−2', '−1', '0', '+1', '+2', '+3'], fontsize = 'large')
        ax[n].set_ylabel('Anomaly in the Niño 3.4 SST, °C', fontsize = 'large', color = [0.5, 0., 0.5])
        ax2 = ax[n].twinx()
        ax2.spines['top'].set_visible(False)
        ax2.spines['bottom'].set_visible(False)
        ax2.spines['left'].set_visible(False)
        ax2.spines['right'].set_visible(False)
        ax2.grid(False)
        ax2.tick_params(length = 6, width = 0.5)
        ax2.set_ylim((-0.03, 0.03))
        ax2.set_yticks(np.arange(-0.03, 0.031, 0.01))
        ax2.set_yticklabels(['−0.03', '−0.02', '−0.01', '0.00', '+0.01', '+0.02', '+0.03'], fontsize = 'large')
        if n == 1:
            ax2.set_ylabel('Anomaly in the relative\nionospheric potential at 13:00 UTC', fontsize = 'large',\
                color = [0., 0., 1.], rotation = 270, va = 'bottom')
            ax[n].plot(xval, ySSTval, color = [0.5, 0., 0.5], clip_on = False, zorder = 4)
            ax[n].fill_between(xval, ySSTval, yzeroval, where = pos, interpolate = True,\
                color = [1., 0.5, 0.5], linewidth = 0.)
            ax[n].fill_between(xval, ySSTval, yzeroval, where = neg, interpolate = True,\
                color = [0.5, 0.9, 0.9], linewidth = 0.)
            ax2.plot(xval, yIP1val, color = [0., 0., 1.], clip_on = False, zorder = 5)
        else:
            ax2.set_ylabel('Anomaly in the relative\nionospheric potential at 21:00 UTC', fontsize = 'large',\
                color = [0., 0., 1.], rotation = 270, va = 'bottom')
            ax[n].plot(xval, ySSTval, color = [0.5, 0., 0.5], clip_on = False, zorder = 4)
            ax[n].fill_between(xval, ySSTval, yzeroval, where = pos, interpolate = True,\
                color = [1., 0.5, 0.5], linewidth = 0.)
            ax[n].fill_between(xval, ySSTval, yzeroval, where = neg, interpolate = True,\
                color = [0.5, 0.9, 0.9], linewidth = 0.)
            ax2.plot(xval, yIP2val, color = [0., 0., 1.], clip_on = False, zorder = 5)
    labels = ['a', 'c', 'b', 'd']
    if n == 0:
        ax[n].text(-(0.18 * w + wt2 - wt1) / w2, 1.02, labels[n], fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
    elif n == 2:
        ax[n].text(-0.18, 1.02, labels[n], fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)
    else:
        ax[n].text(-0.19, 1.02, labels[n], fontsize = 'x-large',\
            fontweight = 'semibold', ha = 'left', va = 'bottom', transform = ax[n].transAxes)

plt.savefig('temporal_ip_variation.eps')

