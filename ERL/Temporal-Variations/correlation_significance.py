from sys import stdin, stdout, stderr
import numpy as np
import scipy.stats as st
import datetime as dt
from math import sqrt

dataIP = np.load('FULL-IP.npy') # contains IP values depending on (d, r, h)
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018
# r (axis 1) is the number of a region (0 is the whole world, 1 to 14 corr. to different areas, 15 is land, 16 is ocean)
# h (axis 2) is the hour of the day (an integer in [0, 24])

dataSST = np.load('FULL-ENSO34.npy') # contains daily values of Niño 3.4 SST depending on d
# d (axis 0) is the number of a day starting with 0 and ending with 1339
# every third day is taken, 0 corresponds to 1 Jan 2008 and 1339 corresponds to 31 Dec 2018

assert dataIP.shape[0] == dataSST.shape[0]

IPs = np.zeros((11, 12, 25)) # IP values for different years and months
SSTs = np.zeros((11, 12))    # Niño 3.4 SST values for different years and months
Ns = np.zeros((11, 12))      # number of days available for different years and months

for i in range(dataIP.shape[0]):
    currdate = dt.date(2008, 1, 1) + dt.timedelta(i * 3)
    IPs[currdate.year - 2008, currdate.month - 1, :] += dataIP[i, 0, :]
    SSTs[currdate.year - 2008, currdate.month - 1] += dataSST[i]
    Ns[currdate.year - 2008, currdate.month - 1] += 1

M = 5 # the number of months used for averaging

stdout.write("TEST BASED ON STUDENT'S T-DISTRIBUTION\n\n")

P = 11 * 12 - (M - 1) # the number of points
a = 0.01 # significance level

q = st.t.ppf(1 - a / 2, P - 2)
r = q / sqrt(q**2 + P - 2)

stdout.write("Significance level: {0:f}\nNumber of points: {1:d}\nCorrelation coefficient: {2:f}\n\n".format(a, P, r))

stdout.write("PERMUTATION TEST\n\n")

linIPs = IPs.reshape((11 * 12, 25))
linSSTs = SSTs.reshape((11 * 12))
linNs = Ns.reshape((11 * 12))
# linear reshapes of the main arrays with respect to months

linIPsum = np.insert(np.cumsum(linIPs, axis = 0), 0, 0, axis = 0)
linSSTsum = np.insert(np.cumsum(linSSTs), 0, 0)
linNsum = np.insert(np.cumsum(linNs), 0, 0)
# cumulative sums over months

IPmeans = (linIPsum[M:, :] - linIPsum[:-M, :]) / (linNsum[M:, np.newaxis] - linNsum[:-M, np.newaxis])
SSTmeans = (linSSTsum[M:] - linSSTsum[:-M]) / (linNsum[M:] - linNsum[:-M])
# mean values over intervals of length M

totIPs = np.concatenate([np.sum(IPs, axis = 0) for _ in range(11)], axis = 0)
totSSTs = np.concatenate([np.sum(SSTs, axis = 0) for _ in range(11)])
totNs = np.concatenate([np.sum(Ns, axis = 0) for _ in range(11)])
# values of variables for all years combined

totIPsum = np.insert(np.cumsum(totIPs, axis = 0), 0, 0, axis = 0)
totSSTsum = np.insert(np.cumsum(totSSTs), 0, 0)
totNsum = np.insert(np.cumsum(totNs), 0, 0)
# cumulative sums over months for all years combined

totIPmeans = (totIPsum[M:, :] - totIPsum[:-M, :]) / (totNsum[M:, np.newaxis] - totNsum[:-M, np.newaxis])
totSSTmeans = (totSSTsum[M:] - totSSTsum[:-M]) / (totNsum[M:] - totNsum[:-M])
# mean values over intervals of length M for all years combined

IPmeans[:, :] /= np.mean(IPmeans[:, :-1, np.newaxis], axis = 1)
totIPmeans[:, :] /= np.mean(totIPmeans[:, :-1, np.newaxis], axis = 1)
# passing to relative values of the IP

IPval = IPmeans[:, :] - totIPmeans[:, :]
SSTval = SSTmeans[:] - totSSTmeans[:]

rng = np.random.default_rng()
N = 1000000 # number of iterations
stdout.write("Number of computations: {0:d}\n\n".format(N))

corr = np.zeros((24))
greater = np.zeros((24))

for h in range(24):
    corr[h] = np.corrcoef(SSTval[:], IPval[:, h])[0, 1]

for _ in range(N):
    SSTperm = rng.permutation(SSTval[:])
    for h in range(24):
        c = np.corrcoef(SSTperm[:], IPval[:, h])[0, 1]
        if abs(c) > abs(corr[h]):
            greater[h] += 1

greater /= N

for h in range(24):
    stdout.write("Hour: {0:02d}:00 UTC\nCorrelation coefficient: {1:f}\np-value: {2:f}\n\n".format(h, corr[h], greater[h]))
